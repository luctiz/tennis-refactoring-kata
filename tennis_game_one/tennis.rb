class Player
  attr_reader :points
  def initialize(name)
    @pName = name
    @points = 0
  end

  def won_point
    @points += 1
  end
  
end


class TennisGame

  def initialize(player1Name, player2Name)
    @player1 = Player.new(player1Name)
    @player2 = Player.new(player2Name)
  end
        
  def won_point(playerName)
    if playerName == "player1"
      @player1.won_point
    else
      @player2.won_point
  end
end
  
  def score
    result = ""
    tempScore=0
    if (@player1.points == @player2.points)
      result = {
          0 => "Love-All",
          1 => "Fifteen-All",
          2 => "Thirty-All",
      }.fetch(@player1.points, "Deuce")
    elsif (@player1.points>=4 or @player2.points>=4)
      minusResult = @player1.points-@player2.points
      if (minusResult==1)
        result ="Advantage player1"
      elsif (minusResult ==-1)
        result ="Advantage player2"
      elsif (minusResult>=2)
        result = "Win for player1"
      else
        result ="Win for player2"
      end
    else
      (1...3).each do |i|
        if (i==1)
          tempScore = @player1.points
        else
          result+="-"
          tempScore = @player2.points
        end
        result += {
            0 => "Love",
            1 => "Fifteen",
            2 => "Thirty",
            3 => "Forty",
        }[tempScore]
      end
    end
    result
  end
end
